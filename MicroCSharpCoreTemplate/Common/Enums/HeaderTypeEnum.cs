﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroCSharpCoreTemplate.Common.Enums
{
    public enum HeaderTypeEnum
    {
        Success,
        Info,
        Functional,
        Technical
    }
}
