﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroCSharpCoreTemplate.Common.Enums
{
    public enum HeaderCodeEnum
    {
        OK_200,
        UA_401,
        EI_500,
        MalformedJSON_301
    }
}
