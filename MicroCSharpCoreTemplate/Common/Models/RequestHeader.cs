﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroCSharpCoreTemplate.Common.Models
{
    public class RequestHeader
    {
        public string Username { get; set; }
        public string Token { get; set; }
    }
}
