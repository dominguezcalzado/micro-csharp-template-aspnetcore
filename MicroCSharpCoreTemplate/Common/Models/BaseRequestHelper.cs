﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace MicroCSharpCoreTemplate.Common.Models
{
    public static class BaseRequestHelper<T>
    {
        public static BaseRequest<T> GetRequest(HttpRequestMessage requestMessage)
        {
            BaseRequest<T> request = new BaseRequest<T>();
            request = JsonConvert.DeserializeObject<BaseRequest<T>>(requestMessage.Content.ReadAsStringAsync().Result);
            return request;
        }

        public static BaseResponse<T> GetResponse(HttpResponseMessage requestMessage)
        {
            BaseResponse<T> response = new BaseResponse<T>();
            response = JsonConvert.DeserializeObject<BaseResponse<T>>(requestMessage.Content.ReadAsStringAsync().Result);
            return response;
        }

        public static (bool, string, RequestHeader) HasRequestHeader(string Username,string Token)
        {
            bool hasHeader = true;
            string error = "";

            if (Username==null)
                return (false, "Header Username can't be null", null);

            if (Token == null)
                return (false, "Header Token can't be null", null);

            RequestHeader header = new RequestHeader();
            header.Username = Username;
            header.Token = Token;

            return (hasHeader, error, header);
        }
    }
}
