﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroCSharpCoreTemplate.Common.Models
{
    public class BaseRequest<T>
    {
        public T Data { get; set; }
    }
}
