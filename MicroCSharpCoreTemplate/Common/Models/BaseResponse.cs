﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroCSharpCoreTemplate.Common.Models
{
    public class BaseResponse<T>
    {
        public HeaderResponse Header { get; set; }
        public T Data { get; set; }
        public BaseResponse()
        {
            Header = new HeaderResponse();
        }
    }

    public class HeaderResponse
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string Type { get; set; }
        public string Details { get; set; }
        public List<ErrorResponse> Errors { get; set; }
        public HeaderResponse()
        {
            Errors = new List<ErrorResponse>();
        }
    }

    public class ErrorResponse
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string Source { get; set; }
        public string Starcktrace { get; set; }
    }
}
