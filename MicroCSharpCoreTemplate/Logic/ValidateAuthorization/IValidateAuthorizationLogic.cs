﻿using MicroCSharpCoreTemplate.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroCSharpCoreTemplate.Logic.ValidateAuthorization
{
    public interface IValidateAuthorizationLogic
    {
        public Task<BaseResponse<bool>> Validate(string Username, string Token);
    }
}
