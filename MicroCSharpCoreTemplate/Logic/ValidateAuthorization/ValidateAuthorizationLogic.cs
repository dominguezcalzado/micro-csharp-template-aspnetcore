﻿using MicroCSharpCoreTemplate.Common.Enums;
using MicroCSharpCoreTemplate.Common.Models;
using MicroCSharpCoreTemplate.Logic.ValidateAuthorization;
using MicroCSharpCoreTemplate.Repository;
using MicroCSharpCoreTemplate.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq; 
using System.Threading.Tasks;

namespace MicroCSharpCoreTemplate.Logic
{
    public class ValidateAuthorizationLogic: IValidateAuthorizationLogic
    {
        private readonly IConnection _repo;
        public ValidateAuthorizationLogic(IConnection repo)
        {
            _repo = repo;
        }
        public async Task<BaseResponse<bool>> Validate(string Username, string Token)
        {
            BaseResponse<bool> response = new BaseResponse<bool>();

            (bool requestHeaderFine, string headerError,
            RequestHeader header) = BaseRequestHelper<bool>.HasRequestHeader(Username, Token);
            if (!requestHeaderFine)
            {
                return new BaseResponse<bool>()
                {
                    Header =
                    {
                        Code = HeaderCodeEnum.OK_200.ToString(),
                        Type = HeaderTypeEnum.Info.ToString(),
                        Message = headerError,
                        Errors = new List<ErrorResponse>(),
                        Details = " ValidateAuthorizationLogic -> Validate"
                    },
                    Data = false
                };
            }


            ConnectionResponse<BaseResponse<bool>> clientResponse = await _repo.ValidateAuthorization<bool>(header.Token, header.Username); ;
            if (!clientResponse.Success)
            {
                return new BaseResponse<bool>()
                {
                    Header =
                    {
                        Code = HeaderCodeEnum.EI_500.ToString(),
                        Type = HeaderTypeEnum.Technical.ToString(),
                        Message = "Ha ocurrido un error al validar la autorización",
                        Errors = new List<ErrorResponse>(),
                        Details = " AuthorizationLogic -> AuthorizeUser"
                    },
                    Data = false
                };
            }
            return clientResponse.Data;


        }
    }
}
