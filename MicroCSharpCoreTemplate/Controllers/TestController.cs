﻿using MicroCSharpCoreTemplate.Common.Models;
using MicroCSharpCoreTemplate.Logic.ValidateAuthorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace MicroCSharpCoreTemplate.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TestController : ControllerBase
    {
        private readonly ILogger<TestController> _logger;
        private readonly IValidateAuthorizationLogic _logic;

        public TestController(ILogger<TestController> logger, IValidateAuthorizationLogic logic)
        {
            _logger = logger;
            _logic = logic;
        }

        [HttpGet]
        [Route("ValidateAuthorization")]
        public async Task<BaseResponse<bool>> ValidateAuthorization([FromHeader] string Username, [FromHeader] string Token)
        {
            BaseResponse<bool> result = await _logic.Validate(Username, Token);

            return result;
        }
    }
}
