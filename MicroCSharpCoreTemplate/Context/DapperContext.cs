﻿using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace MicroCSharpCoreTemplate.Context
{
    public class DapperContext
    {
        private readonly IConfiguration _configuration;
        public DapperContext(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public IDbConnection CreateConnection()
            => SelectDataBase();


        private dynamic SelectDataBase()
        {
            switch (_configuration.GetSection("Settings").GetSection("DefaultConnection").Value)
            {
                case "PostgresPrimaryConnection":
                    return new NpgsqlConnection(_configuration.GetConnectionString("PostgresPrimaryConnection"));
                case "PostgresSecundaryConnection":
                    return new NpgsqlConnection(_configuration.GetConnectionString("PostgresSecundaryConnection"));
                case "SQLPrimaryConnection":
                    return new SqlConnection(_configuration.GetConnectionString("SQLPrimaryConnection"));
                case "SQLSecundaryConnection":
                    return new SqlConnection(_configuration.GetConnectionString("SQLSecundaryConnection"));
                default:
                    return new NpgsqlConnection(_configuration.GetConnectionString("PostgresPrimaryConnection"));
            }
        }
    }
}
