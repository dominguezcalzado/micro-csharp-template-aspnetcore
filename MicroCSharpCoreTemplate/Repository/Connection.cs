﻿using Dapper;
using MicroCSharpCoreTemplate.Common.Models;
using MicroCSharpCoreTemplate.Context;
using MicroCSharpCoreTemplate.Repository.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace MicroCSharpCoreTemplate.Repository
{
    public class Connection : IConnection
    {
        private readonly DapperContext _context; 
        private readonly IConfiguration _configuration;


        public Connection(DapperContext  context, IConfiguration configuration)
        {
            _configuration = configuration;
            _context = context; 
        }


        public  ConnectionResponse<List<T>> DapperExecuteList<T>(string consulta, List<KeyValuePair<string, object>> parametros = null)
        {
            ConnectionResponse<List<T>> connectionResponse = new ConnectionResponse<List<T>>();

            try
            {
                using (var connection = _context.CreateConnection())
                {
                    List<T> result = connection.Query<T>(consulta, parametros, commandType: CommandType.Text).ToList();
                    connectionResponse.Data = result;
                    connectionResponse.Success = true;
                    connectionResponse.Error = null;
                    connectionResponse.ErrorMessage = null;
                    connection.Close();
                }

            }
            catch (Exception e)
            {
                connectionResponse.Data = new List<T>();
                connectionResponse.Success = false;
                connectionResponse.Error = e.StackTrace;
                connectionResponse.ErrorMessage = e.Message;
                return connectionResponse;
            }

            return connectionResponse;

        }

        public ConnectionResponse<bool> DapperExecute(string consulta, List<KeyValuePair<string, object>> parametros = null)

        {
            ConnectionResponse<bool> connectionResponse = new ConnectionResponse<bool>();
            try
            {
                using (var connection = _context.CreateConnection())
                {
                    connection.Execute(consulta, parametros, commandType: CommandType.Text);
                    connection.Close();
                    connectionResponse.Data = true;
                    connectionResponse.Success = true;
                    connectionResponse.Error = null;
                    connectionResponse.ErrorMessage = null;
                }
            }
            catch (Exception e)
            {
                connectionResponse.Data = false;
                connectionResponse.Success = false;
                connectionResponse.Error = e.StackTrace;
                connectionResponse.ErrorMessage = e.Message;
                return connectionResponse;
            }
            return connectionResponse;
        }
        public  ConnectionResponse<T> DapperSingle<T>(string consulta, List<KeyValuePair<string, object>> parametros = null)

        {
            ConnectionResponse<T> connectionResponse = new ConnectionResponse<T>();

            try
            {
                using (var connection = _context.CreateConnection())
                {
                    T result = connection.Query<T>(consulta, parametros, commandType: CommandType.Text).FirstOrDefault();
                    connectionResponse.Data = result;
                    connectionResponse.Success = true;
                    connectionResponse.Error = null;
                    connectionResponse.ErrorMessage = null;
                    connection.Close();
                }

            }
            catch (Exception e)
            {
                connectionResponse.Data = default(T);
                connectionResponse.Success = false;
                connectionResponse.Error = e.StackTrace;
                connectionResponse.ErrorMessage = e.Message;
                return connectionResponse;
            }
            return connectionResponse;
        }
        public async Task<ConnectionResponse<BaseResponse<T>>> ValidateAuthorization<T>(string token, string username)
        {
            ConnectionResponse<BaseResponse<T>> connectionResponse = new ConnectionResponse<BaseResponse<T>>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_configuration.GetSection("Settings").GetSection("MicroAuthorizationURL").Value);
                    client.DefaultRequestHeaders.Add("Token", token);
                    client.DefaultRequestHeaders.Add("Username", username);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = await client.GetAsync("/api/authorization/ValidateAuthorization");
                    connectionResponse.Data = BaseRequestHelper<T>.GetResponse(response);
                    connectionResponse.Success = true;
                    connectionResponse.Error = null;
                    connectionResponse.ErrorMessage = null;
                }
            }
            catch (Exception e)
            {
                connectionResponse.Data = default(BaseResponse<T>);
                connectionResponse.Success = false;
                connectionResponse.Error = e.StackTrace;
                connectionResponse.ErrorMessage = e.Message;
                return connectionResponse;
            }

            return connectionResponse;
        }
    }
}
