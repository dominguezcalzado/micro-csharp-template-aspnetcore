﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroCSharpCoreTemplate.Repository.Models
{
    public class ConnectionResponse<T>
    {
        public bool Success { get; set; }
        public T Data { get; set; }
        public string Error { get; set; }
        public string ErrorMessage { get; set; }

    }
}
