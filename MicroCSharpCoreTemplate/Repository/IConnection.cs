﻿using MicroCSharpCoreTemplate.Common.Models;
using MicroCSharpCoreTemplate.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroCSharpCoreTemplate.Repository
{
    public interface IConnection
    {
        public ConnectionResponse<List<T>> DapperExecuteList<T>(string consulta, List<KeyValuePair<string, object>> parametros = null);
        public ConnectionResponse<bool> DapperExecute(string consulta, List<KeyValuePair<string, object>> parametros = null);
        public ConnectionResponse<T> DapperSingle<T>(string consulta, List<KeyValuePair<string, object>> parametros = null);

        public Task<ConnectionResponse<BaseResponse<T>>> ValidateAuthorization<T>(string token, string username);
    }
}
